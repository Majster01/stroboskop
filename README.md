# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://Majster01@bitbucket.org/Majster01/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/Majster01/stroboskop/commits/3f369ae23a0bad42a699297e6063e923dda8c29a

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Majster01/stroboskop/commits/fff958c436935d69302586fd5cc4b7d7b2cda91d

Naloga 6.3.2:
https://bitbucket.org/Majster01/stroboskop/commits/f0daef2212dcd6830683f228fe7f235f12375685

Naloga 6.3.3:
https://bitbucket.org/Majster01/stroboskop/commits/8940df72fcfc381ad79674ecdb86894acc07d99f

Naloga 6.3.4:
https://bitbucket.org/Majster01/stroboskop/commits/1d85349c8ec0b0ebb97ad22e9849adef1f510b12

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/Majster01/stroboskop/commits/5731869a89804896bea8c8e423973ce504aa2996

Naloga 6.4.2:
https://bitbucket.org/Majster01/stroboskop/commits/0e2514751a49d0f9b4a407e187c3682db9090338

Naloga 6.4.3:
https://bitbucket.org/Majster01/stroboskop/commits/e3484b72a38fcea3e4c51ea723b5f011fdb3ef00

Naloga 6.4.4:
https://bitbucket.org/Majster01/stroboskop/commits/890ff256112a3d5cb0f37b35c68bf7836576664e